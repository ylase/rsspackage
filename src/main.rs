use std::error::Error;
use std::fs::{File, OpenOptions};
use std::io::Write;
use std::net::TcpStream;
use std::path::Path;
use std::sync::Arc;
use imap::Session;
use imap::types::UnsolicitedResponse;
use lettre::{Message, SmtpTransport, Transport};
use native_tls::TlsStream;
use rss::Channel;
use log::{info, LevelFilter};
use mailparse::MailHeaderMap;
use serde_json::{Value, json};
use serde::{Deserialize, Serialize};
use simplelog::*;
use tokio::task::JoinHandle;
use failure::Fail;
use futures::future::join_all;
use lettre::transport::smtp::authentication::Credentials;

#[derive(Fail, Debug)]
#[fail(display = "There was an error during email parsing. See logs.")]
struct FetchError;

#[derive(Serialize, Deserialize, Clone)]
struct IMAPConfig {
    email_address: String,
    password: String,
    imap_domain: String,
    port: u16,
}
#[derive(Serialize, Deserialize, Clone)]
struct SMTPConfig {
    email_address: String,
    password: String,
    smtp_domain: String,
    port: u16,
}
#[derive(Serialize, Deserialize, Clone)]
struct RSSPacketConfig {
    imap_info: IMAPConfig,
    smtp_info: SMTPConfig,
    max_packet: usize
}
// serde_json likes when you have structs to assign a json object to.

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    CombinedLogger::init(
        vec![
            TermLogger::new(LevelFilter::Warn, Config::default(), TerminalMode::Mixed, ColorChoice::Auto),
            TermLogger::new(LevelFilter::Info, Config::default(), TerminalMode::Mixed, ColorChoice::Auto),
        ]
    ).unwrap();
    info!("Logger init");

    let default_json: Value = json!( // the exact same as above structs
    {
        "imap_info": {
            "email_address": "user@example.com",
            "password": "password",
            "imap_domain": "imap.example.com",
            "port": 992u16,
        },
        "smtp_info": {
            "email_address": "user@example.com",
            "password": "password",
            "smtp_domain": "smtp.example.com",
            "port": 465u16,
        },
        "max_packet": 5120usize
    }
    );

    let config_path = std::env::current_exe().unwrap().parent().unwrap()
        .join(Path::new("rsspacket_config.json")); // i dont care about errors

    info!("Got config path at {}", &config_path.to_str().unwrap()); // the config file should be flat right next to the executable

    fn config_file(config_path: &Path) -> Result<File, std::io::Error> { OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(config_path) } // Very overcomplicated way to open a config file.

    let config: RSSPacketConfig = match serde_json::from_reader(config_file(&config_path)?){
        Ok(config) => config,
        Err(_) => {
            match config_file(&config_path)?.write(default_json.to_string().as_bytes()) {
                Ok(_) => {}
                Err(_) => {warn!("Error writing default config file")}
            }
            warn!("No config found, writing default config file and exiting. Remember to change it");
            std::process::exit(1);
        }
    };

    let mut imap_session =
        imap_login(&*config.imap_info.imap_domain,
                   &*config.imap_info.email_address,
                   &*config.imap_info.password,
                   config.imap_info.port)?; // whoa this almost looks like C++

    imap_session.select("INBOX")?;

    loop {
        match imap_session.idle().keepalive(true).wait_while(|response| {
            info!("Server broke idle");
            // IMAP IDLE is super interesting and powerful, but I'm not using all of that power.
            // Instead, I just make sure my connection is still active and then search for new emails.
            // It will hold until the server tells us that something has happened.
            match response {
                UnsolicitedResponse::Bye { code: _, information} =>
                    {
                        warn!("Server disconnected with reason {}", information.unwrap());
                        return false;
                    }
                _ => {} // Stop idling if the server disconnected, otherwise keep going.
            }

            return false;
        }) {
            Ok(_) => {}
            Err(e) => {
                warn!("Unknown error {} in idle", e);
            }
        };
        // After the idle has ended, we can do a search for the new emails...

        info!("Searching for new emails");
        let emails = match imap_session.search("NEW") { // Searches for any recent unseen emails.
            Ok(emails) => { emails }
            Err(e) => {
                warn!("Unexpected error in search {}", e);
                continue;
            } // If the search borks, just wait for the next update
        };

        let mut emails_iter = emails.iter().peekable();
        let mut sequences_buffer = String::new();
        let sequences = loop { // imap fetch wants a comma separated list of email ids. fuck you, imap fetch
            if emails_iter.peek().is_some() { break sequences_buffer; }

            sequences_buffer.push_str(&*emails_iter.next().unwrap().to_string());
            if emails_iter.peek().is_some() { sequences_buffer.push_str(","); } // Cursed but makes sure it doesnt break
        };

        info!("Fetching those emails");
        // And fetch the new emails...
        let fetches = match imap_session.fetch(sequences, "(BODY[])") {
            Ok(fetches) => { Arc::new(fetches) }
            Err(e) => {
                warn!("Unexpected error in fetch {}", e);
                continue;
            }
        };

        let mut handle_set = Vec::<JoinHandle<Result<Message, FetchError>>>::new();
        // Iterate over all the new emails and uhhh do stuff
        for fetch in fetches.iter() {
            info!("Processing email {}", fetch.uid.unwrap());

            // Lifetime bullshit doesn't let us access the email from a future directly.
            let header = match fetch.header() {
                None => {
                    warn!("How did you get here? There's no envelope. Your email server is screwed up");
                    Vec::<u8>::new()
                } // Will never happen
                Some(header) => { header.to_owned().clone() }
            };
            let body_bytes = match fetch.text() {
                None => {
                    warn!("This email has no text!");
                    Vec::<u8>::new()
                }
                Some(txt) => { txt.to_owned().clone() }
            };


            // So we can move it into the async
            // Even though it is only referenced, it still needs a static lifetime
            let config_clone = config.clone();

            let handle: JoinHandle<Result<Message, FetchError>> = tokio::spawn(async move {
                // The header was gotten up there.
                if header == Vec::<u8>::new() {return Err(FetchError);} // If it failed up there, we need to error here so it knows what handle failed.
                let (parsed, _) = match mailparse::parse_headers(&header[..]) {
                    Ok(parsed) => {parsed}
                    Err(e) => {
                        warn!("Invalid header {}", e);
                        return Err(FetchError);
                    }
                };

                let sender = match parsed.get_first_value("From") {
                    None => {
                        warn!("This email has no sender!");
                        return Err(FetchError);
                    }
                    Some(sender) => {sender}
                };

                // Get the body as bytes then convert it to a str. If it's not UTF8 it breaks.
                // The body was gotten outside the async because bullshit lifetimes.
                if body_bytes == Vec::<u8>::new() {return Err(FetchError);} // If it failed up there, we need to error here so it knows what handle failed.
                let body = match std::str::from_utf8(&body_bytes[..]) {
                    Ok(v) => { v },
                    Err(e) => {
                        warn!("Invalid UTF-8 sequence: {}", e);
                        return Err(FetchError);
                    },
                };

                let response_text = match process_rss(&body, &config_clone.max_packet).await {
                    Ok(resp) => { resp },
                    Err(e) => {
                        warn!("Bad RSS feed: {} due to error: {}",  &body, e);
                        return Err(FetchError);
                    }
                };

                return match Message::builder()
                    .from((&config_clone.smtp_info.email_address).parse().unwrap())
                    .to(sender.parse().unwrap())
                    .body(response_text) {
                    Ok(message) => { Ok(message) }
                    Err(e) => {
                        warn!("Error in message building: {}", e);
                        Err(FetchError)
                    }
                };

            });
            handle_set.push(handle);

        }

        // Now loop over those futures and send emails when they return
        let mailer = SmtpTransport::relay(&config.smtp_info.smtp_domain)
            .unwrap()
            .port(*&config.smtp_info.port)
            .credentials(Credentials::new(
                (&config.smtp_info.email_address).parse().unwrap(),
                (&config.smtp_info.password).parse().unwrap()))
            .build();

        // TODO make this operate on a case-by-case instead of once all the threads are done
        let joined_messages = join_all(handle_set).await; // Probably real slow but oh well

        for message in joined_messages {
            match message {
                Ok(message) => { match message {
                    Ok(message) => { match mailer.send(&message) {
                        Ok(_) => {info!("Successfuly sent email!")}
                        Err(e) => {warn!("Error sending email: {}", e)}
                    }
                    }
                    Err(_) => {warn!("Error in processing email. See log above")}
                }
                }
                Err(e) => {warn!("Tokio error in message future {}", e)}
            }
        }
    }
}

fn imap_login(domain: &str, address: &str, password: &str, port: u16) -> Result<Session<TlsStream<TcpStream>>, imap::Error> {
    // copied from the imap crate page lol

    let attempt = imap::ClientBuilder::new(domain, port).native_tls();

    let client = match attempt {
            Ok(res) => { res }
            Err(e) => { warn!("Failed to connect to server: {}", e); return Err(e);}
    };

    let session = match client.login(address, password) {
        Ok(res) => { res },
        Err(e) => { warn!("Failed to connect to server" ); return Err(e.0);}
    };

    return Ok(session);
}

// Pls multithread this it does slow stuff like networking obviously
async fn process_rss(url: &str, packet_limit: &usize) -> Result<String, Box<dyn Error>> {
    let stream = reqwest::get(url).await?.bytes().await?; // I mean if it errors what are we gonna do
    let channel = Channel::read_from(&stream[..])?; // Syntactic rat poison
    info!("Got RSS stream from: {}", url);

    let mut buffer = String::new();

    buffer.push_str(channel.title());
    buffer.push_str("\n");

    for item in channel.items {
        let item_title = match item.title.as_ref() {
            Some(title) => title.clone(),
            None => "NO_TITLE".to_string()
        };

        let item_desc = match item.description.as_ref() {
            Some(content) => content.clone() + "\n",
            None => "".to_string()
        };

        let item_content = match item.content.as_ref() {
            Some(content) => content.clone() + "\n",
            None => "".to_string()
        };


        let item_buf = item_title + "\n" + &item_desc + &item_content;

        if (buffer.clone() + &item_buf).len() >= *packet_limit {
            break;
        } else {
            buffer.push_str(&item_buf);
        }
    }

    info!("Processed RSS stream from: {}", url);
    return Ok(buffer);
}