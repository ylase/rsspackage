RSSPACKAGE
==========
A simple server written in rust to allow accessing of RSS feeds via email.

WARNING
-------
Currently not tested, likely full of bugs to the point of unusability.
Also lacking in features.
Has an embarassingly large amount of dependencies, `cargo` will download a gigabyte or so of sources.

CONFIGURATION
-------------
On first run, the binary creates a `rsspackage_config.json`. This file is mostly self explanatory:
The `imap` and `smtp` settings refer to the login email and password, the respective protocol server, and the port on which the connection is made.
Please note that `rsspackage` currently requires that TLS/SSL is used. I couldn't figure out how to remove that.
The `max_packet` option configures the maximum CHARACTERS of the formatted response. If you're fetching CJK feeds, your response will have much more bytes for the same `max_packet` option.

USAGE
-----
Currently, `rsspacket` simply treats the entire email body as a UTF-8 encoded link to an RSS feed.
It will fetch the title of the feed, and then recursively add the title, description, and content of each article until the maximum number of characters has been reached.

TODO
----
1) Make sure it functions
2) Allow users to request specific articles or formatting, such as only showing titles of articles so the user can pick which one they want.